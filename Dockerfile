FROM openjdk:8-jre-alpine AS base

ARG artifactory_user
ARG artifactory_pass

RUN apk add jq less bash openssl openssh curl wget

# Git ssh setup
RUN mkdir $APP_HOME
RUN mkdir ~/.ssh
